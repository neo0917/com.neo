package com.neo.country.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.neo.country.service.UserService;
import com.neo.country.service.UserServiceImpl;
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	@Autowired UserService userService;
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
         http
              .csrf().disable()
              .authorizeRequests()
              
              .antMatchers("/user").hasAuthority("USER")
              .antMatchers("/admin").hasAuthority("ADMIN")
              
                   .anyRequest().authenticated()
                   .and()
              .formLogin()
              
              .and()
              .logout()
              
              ;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//         auth.userDetailsService(userService).passwordEncoder(this.noOpPasswordEncoder());
         auth.userDetailsService(userService).passwordEncoder(userService.passwordEncoder());
    }	
    
    @Bean
    public PasswordEncoder  noOpPasswordEncoder() {
    	return NoOpPasswordEncoder.getInstance();
    }    
}
