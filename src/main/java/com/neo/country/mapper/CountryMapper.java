package com.neo.country.mapper;

import java.util.List;

import com.neo.country.domain.Country;
import com.neo.country.domain.Home;

public interface CountryMapper {
	public List<Country> getCountryList();

	public Home readHome();
}
