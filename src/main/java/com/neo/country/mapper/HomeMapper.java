package com.neo.country.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.neo.country.domain.Home;

@Mapper
public interface HomeMapper {
	public Home readHome(@Param("name") String name);
}
