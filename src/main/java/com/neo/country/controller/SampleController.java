package com.neo.country.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.neo.country.domain.Country;
import com.neo.country.domain.Home;
import com.neo.country.mapper.HomeMapper;
import com.neo.country.service.CountryService;

@RestController
public class SampleController {
	
	@Autowired
	private CountryService countryService;
	
	@Autowired 
	private HomeMapper homeMapper;
	
	@RequestMapping(value = "/")
	@ResponseBody
	public String getSample() {
		return "Sample";
	}

	@RequestMapping(method = RequestMethod.GET, value="/country")
	@ResponseBody
	public List<Country> getCountryList() {
		List<Country> countryList = countryService.getCountryList();
		
		return countryList;
	}
	
//	@RequestMapping("/{name}")
//	public String home(@PathVariable String name) {
////	public Home home(@PathVariable String name) {
//		System.out.println("name : "+name);
////		Home home = homeMapper.readHome(name);
////		Home home = countryService.readHome(name);
////		return home;
//		return "This is "+name+"page";
//	}
		
	@RequestMapping("/admin") 
	public String admin() { return
	  "This is admin page"; }
	  
	@RequestMapping("/user") 
	public String user() { return "this is user page"; }
	 

}

