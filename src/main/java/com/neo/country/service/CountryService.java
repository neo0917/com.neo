package com.neo.country.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.neo.country.domain.Country;
import com.neo.country.domain.Home;
import com.neo.country.mapper.CountryMapper;

@Service
public class CountryService {
	@Autowired
	private CountryMapper countryMapper;
	
	public List<Country> getCountryList(){
		return countryMapper.getCountryList();
	}

	public Home readHome(String name) {
		// TODO Auto-generated method stub
		return countryMapper.readHome();
	}
}
